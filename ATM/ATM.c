#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <string.h>
	
unsigned long cantidad = 10000; 


	 int buscar_tarjeta (char *n_tarjeta, int pin, char *n_cuenta, char *user){
	
	 FILE *pf = NULL;
	 int aux_pin;
	 char * aux_user= malloc (50* sizeof (char));
	 char *aux_n_tarjeta= malloc (16* sizeof (char));
	 char *aux_n_cuenta=malloc (20*sizeof(char)); 	
	
    if ( !(pf = fopen ("tarjetas.txt", "r")) ) {
        fprintf (stderr, "No puede abrir el fichero.\n");
        return EXIT_FAILURE;
    }
    
    while (fscanf (pf,"%s %i %s %s", aux_n_tarjeta, &aux_pin, aux_n_cuenta, aux_user) != EOF) {
  	
    	if (strcmp (aux_n_tarjeta, n_tarjeta) == 0 && aux_pin == pin ) {
		 	strcpy (n_cuenta, aux_n_cuenta);
		 	strcpy (user, aux_user);
		 
		 return 1;
		 } 
    	
    } 

    fclose (pf); 
    return 0;
}


int leer_cuenta (char *n_cuenta, char *user, int *saldo){
	
	 FILE *pf = NULL;
	 char *aux_n_cuenta=malloc (20*sizeof(char)); 	
	 char *aux_user= malloc (50* sizeof (char));
	 int aux_saldo;
	
    if ( !(pf = fopen ("cuentas.txt", "rw") ) ){
        fprintf (stderr, "No puede abrir el fichero.\n");
        return EXIT_FAILURE;
    }
    
    while (fscanf (pf,"%s %s %i",aux_n_cuenta, aux_user, &aux_saldo) != EOF) {
  	
    	if (strcmp (aux_n_cuenta, n_cuenta) == 0) {
		 	saldo = aux_saldo;
		 return 1;
		} 
   
    } 

    fclose (pf); 
   return 0; 
}

void menu () {
	int pin;
	char *n_tarjeta= malloc (16* sizeof (char));
	char *n_cuenta= malloc (20* sizeof (char));
	char *user= malloc (50* sizeof (char));
	unsigned long retirar, ingresar;
	int op, saldo;
	int fondo[5], bandeja[5];
	
	fondo[0]=10;
	fondo[1]=30;
	fondo[2]=50;
	fondo[3]=50;
	fondo[4]=40;
	
	bandeja[0]=0;
	bandeja[1]=0;
	bandeja[2]=0;
	bandeja[3]=0;
	bandeja[4]=0;
		
	printf ("\t\t\t\tCAJERO\n");
	
		printf ("Ingrese el numero de tarjeta.\n");
		scanf ("%s", n_tarjeta);
		printf ("%s\n", n_tarjeta);

		printf ("Ingrese el numero pin para continuar.\n");
		scanf ("%i", &pin);
		printf ("%i\n", pin);
		
	
	do {
		if (buscar_tarjeta (n_tarjeta, pin, n_cuenta, user)){
		leer_cuenta (n_cuenta, user, &saldo);
			
		system ("cls"); 
		printf ("1. Retirar dinero.\n ");
		printf ("2. Ingresar dinero.\n ");
		printf ("3. Balance de cuenta\n ");
		printf ("4. Salir.\n");
		scanf ("%i", &op);
		
		switch (op){
			case 1: //SACAR DINERO
				printf ("Cantidad de dinero a retirar: \n");
				scanf ("%lu", &retirar);
				if (retirar % 5 != 0) {
					printf ("Solo validos multiplos de 5.\n");
				system("pause");
				} 
				else if (saldo > retirar){
					printf ("Saldo insuficiente.\n");
					system("pause");
				}
				else {
					/*Llamar al algoritmo de comprobación y saldo en función a parte*/
					/*invocar funcion: sacar_dinero(retirar, &fondo, &bandeja)*/
					sacar_dinero(retirar, fondo, bandeja);
					
					printf("Le entregamos %i billetes de 100 euros\n", bandeja[0]);
					printf("Le entregamos %i billetes de 50 euros\n", bandeja[1]);
					printf("Le entregamos %i billetes de 20 euros\n", bandeja[2]);
					printf("Le entregamos %i billetes de 10 euros\n", bandeja[3]);
					printf("Le entregamos %i billetes de 5 euros\n", bandeja[4]);
				
					system("pause");
				
					/*saldo = saldo - retirar; a falta dificarlo e moden el fichero.*/
					
				}
			break;
			
			case 2: //INGRESAR
				printf ("Cantidad de dinero a ingresar:\n");
				scanf ("%lu", &ingresar);
				break;
			}
		}
	}  while (op < 4);
}

int sacar_dinero (int cantidad, int fondo[5], int bandeja[5]){
		
		int disponible, cociente, resto;
		
		disponible = fondo[0] * 100 + fondo[1] * 50 + fondo[2] * 20 + fondo[3] * 10 + fondo[4] * 5;
		printf ("El disponible total es de: %i\n", disponible);
	
	if (cantidad > disponible) {
		printf ("El cajero no dispone del saldo suficiente, por favor recargalo.\n");
		return EXIT_FAILURE;	
	}
	
	else {
		bandeja[0] = cantidad / 100; 
		fondo[0] = fondo[0] - bandeja[0];
		cantidad = cantidad % 100;
			
		bandeja[1] = cantidad / 50; 
		fondo[1] = fondo[1] - bandeja[1];
		cantidad = cantidad % 50;
			
		bandeja[2] = cantidad / 20; 
		fondo[2] = fondo[2] - bandeja[2];
		cantidad = cantidad % 20;
			
		bandeja[3] = cantidad / 10; 
		fondo[3] = fondo[3] - bandeja[3];
		cantidad = cantidad % 10;
			
		bandeja[4] = cantidad / 5; 
		fondo[4] = fondo[4] - bandeja[4];
		cantidad = cantidad % 5;
		
		return 0;
	}
}

int main (int argc, char *argv[]) {   
    menu ();
    
    
	return EXIT_SUCCESS;
}
