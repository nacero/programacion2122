#include <stdio.h>
#include <stdlib.h>

#define MAX 128
#define MIN 32

int main () {
	
	int num;
	char numchar[3];
	printf ("Introduce un numero entre 32 y 128\n");
	scanf ("%d", &num);
	
	if (num<MIN || num>MAX) {
		printf ("Ese numero no esta entre los establecidos.");
	}
	else {
		printf ("El numerico es: %d\n", num);
		sprintf (numchar, "%s", num);
		printf ("Numero como caracter: %s\n", numchar); // a revisar...
	}
return EXIT_SUCCESS;
}




