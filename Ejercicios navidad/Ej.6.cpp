#include <stdio.h>
#include <stdlib.h>

#define CYAN_T     "\x1b[36m"
#define CYAN_F     "\x1b[46m"
#define VERDE_T	   "\x1b[32m"
#define VERDE_F    "\x1b[42m"


int main () {
	
	int num1, num2, resul;
	
	printf ("Introduce el primer numero:");
	scanf ("%i", &num1);

	printf ("Introduce el segundo numero:");
	scanf ("%i", &num2);
	
	resul = num1 + num2;

	printf (CYAN_T "%i+%i =" VERDE_T "i%\n", num1, num2, resul);

return EXIT_SUCCESS;
}
