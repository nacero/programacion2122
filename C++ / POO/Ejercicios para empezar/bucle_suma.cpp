#include <iostream>

using namespace std;

int main ()

    {
        int x, y, sum=0;
        bool bandera = true;
        char resp;

        while (bandera){

            cout<<"Ingrese el primer numero a sumar: "; cin>>x;
            cout<<"Ingrese el segundo numero a sumar: "; cin>>y;

            sum = x + y;

            cout<<"La suma es = "<<sum<<endl;
            cout<<"¿Desea sumar otros dos numeros?" <<endl;
            cout<<"(S/N) "; cin>>resp;

            if ((resp == 'n') || (resp =='N'))
                bandera = false;
        }

        return 0;

    }
