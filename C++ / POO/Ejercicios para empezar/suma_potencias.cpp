#include <iostream>
#include <math.h>
using namespace std;

int main ()
{
    int resul =0, elevado = 0, n;

    cout<<"Ingrese la cantidad de elementos a sumar:"; cin>>n;

    for (int i = 1; i<=n; i++)
    {
        elevado = pow(2,i);
        resul += elevado;
    }

    cout<<"La suma de las potencias es: "<<resul;

    return 0;
}
