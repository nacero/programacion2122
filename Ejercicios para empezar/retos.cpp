#include <stdio.h>
#include <stdlib.h>

#define CYAN "\x1B[36m"
int main ()
{
	int op1;

	printf ("Ingresa un numero:\n");
	scanf ("%i", &op1);

	/*JUSTIFICACION A LA DERECHA*/
	
	printf ("JUSTIFICACION\n\n");
	printf ("%6i\n\n", op1);

	/*LOS CEROS*/

	printf ("CEROS\n\n");
	printf ("%012i\n\n", op1);

	
	/*TABULAOR*/
	
	printf ("TABULACION\n\n");
	printf ("\t%i\n\n", op1);

	
	/*C0LORES*/

	printf ("COLORES\n\n");	
	printf("%s %6i\n\n", CYAN, op1);

	return EXIT_SUCCESS;
}
