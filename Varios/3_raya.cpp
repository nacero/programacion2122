
#include <stdio.h>
#include <stdlib.h>

#define MAX 3
#define VACIO 0

void 
relleno (unsigned tab[MAX][MAX]){
	for (int f=0; f<MAX; f++){
		for (int c=0; c<MAX; c++){
			tab [f][c]=VACIO;
		}
	}
}

void 
imprimir (unsigned tab[MAX][MAX]){

	for (int f=0; f<MAX; f++){
		printf ("\t -------------");
		printf ("\n\t | ");
		for (int c=0; c<MAX; c++){
			printf("%u | ", tab[f][c]);
		}	
		printf("\n");
	}
	printf ("\t -------------");
	printf ("\n");
}


int main () {

	unsigned tab[MAX] [MAX];

	relleno (tab);
	imprimir (tab);

	return EXIT_SUCCESS;
}
