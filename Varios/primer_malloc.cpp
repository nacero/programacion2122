#include <stdio.h>
#include <stdlib.h>

#define MAX 15

int main () {

	char p1[MAX];
	char *p2;

printf ("Primera frase:");
scanf ("%s", p1);

printf ("Segunda frase:");
scanf ("%ms", &p2);

printf ("\n\n");

printf ("Primera frase: %s\n", p1);
printf ("Segunda frase: %s\n", p2);

free(p2);

return EXIT_SUCCESS;
}
