
#include <stdio.h>
#include <stdlib.h>

int main () {

	unsigned nceldas;
	double *v;

	printf ("Dimensión del vector: ");
	scanf ("%u", &nceldas);    //la dirección de...

	v = (double*) malloc (nceldas * sizeof (double));

	for (unsigned p=0; p<nceldas; p++){    //rellenar el array
		printf ("V[%u] = ", p+1);
		scanf ("%lf", v + p); //sumamos 8 bits ya que es un double. no sumamos de uno en uno

	}
	
	printf ("\n");

	for (unsigned p=0; p<nceldas; p++)
		printf ("%.2lf\n", *(v+p));  //El contenido de la celda.
	printf ("\n");

	free (v);

	return EXIT_SUCCESS;
}
