
//programa que pide dos numero desde una funcion externa y los sume desde otra f. externa con retorno.

#include <stdio.h>
#include <stdlib.h>

void
pedir_n (unsigned *n1,unsigned *n2){  // PASO POR REFERENCIA.

	printf ("Ingrese un primer numero:");
	scanf ("%u", n1);
	printf ("Ingrese un segundo  numero:");
	scanf ("%u", n2);
}


unsigned sumar (unsigned n1, unsigned n2){ // PASO POR VALOR CON RETORNO.
	
	return n1 + n2;
}


void restar (unsigned n1, unsigned n2){ //PASO POR VALOR SIN RETORNO.

	unsigned resul = n1 - n2;

	printf ("El numero %u - %u = %u\n", n1, n2, resul);

}


int main () {

unsigned n1, n2;

	pedir_n (&n1, &n2);
	
	printf("La suma de %u + %u = %u\n", n1, n2, sumar (n1, n2));

restar (n1, n2);
return EXIT_SUCCESS;
}
