
#include <stdio.h>
#include <stdlib.h>

void preguntar (int *preg) {
    printf ("Escribe un numero: ");
    scanf ("%u", preg);
}

int main (){

    int preg;
    preguntar (&preg);

    for (int fil=0; fil<preg; fil++){
        for (int fil2=0; fil2<preg; fil2++){
            for (int col=0; col<preg; col++){
                for (int col2=0; col2<preg; col2++){
                    if ((fil+col)%2 != 0)
                        printf ("* ");
                    else 
                        printf ("0 ");
                }
            }
            printf ("\n");
        }
    }
    return EXIT_SUCCESS;
}
