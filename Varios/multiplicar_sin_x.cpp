
//Este programa pide un  numero al usuario para multiplicarlo por dos pero no se pueden utilizar operadores.

#include <stdio.h>
#include <stdlib.h>

int main (){

    unsigned num, resul; //Asignación de variables de tipo unsigned.

    printf("Introduce el numero a multiplicar: \n");
    scanf ("%u", &num);

    resul = num << 1; //Esto mueve un bit hacia la izquierda por lo que multiplica el numero original por 2.

    printf ("El resultado es: %u\n", resul);

return EXIT_SUCCESS;
}

